FROM python:3.7
ENV PYTHONPATH="$PYTHONPATH:/app"
ADD . /app
WORKDIR /app/address_suggester
RUN pip install -r requirements.txt
CMD python app.py
