db.objects.createIndex({
        "name_ru": "text",
        "analyzed_ru.region": "text",
        "analyzed_ru.raion": "text",
        "analyzed_ru.city": "text",
        "analyzed_ru.district": "text",
        "analyzed_ru.microdistrict": "text",
        "analyzed_ru.street": "text",

        "name_ua": "text",
        "analyzed_ua.region": "text",
        "analyzed_ua.raion": "text",
        "analyzed_ua.city": "text",
        "analyzed_ua.district": "text",
        "analyzed_ua.microdistrict": "text",
        "analyzed_ua.street": "text",
        "analyzed_ua.address": "text"
    },
    {
        weights: {
            "name_ru": 1,
            "analyzed_ru.region": 60,
            "analyzed_ru.raion": 50,
            "analyzed_ru.city": 40,
            "analyzed_ru.district": 30,
            "analyzed_ru.microdistrict": 20,
            "analyzed_ru.street": 10,
            "analyzed_ru.address": 1,

            "name_ua": 1,
            "analyzed_ua.region": 60,
            "analyzed_ua.raion": 50,
            "analyzed_ua.city": 40,
            "analyzed_ua.district": 30,
            "analyzed_ua.microdistrict": 20,
            "analyzed_ua.street": 10,
            "analyzed_ua.address": 1
        },
        name: "MyTextIndex"
});
