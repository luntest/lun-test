import logging
from enum import Enum
import typing

import pymongo.collection
from pymongo.cursor import Cursor
from pymongo.errors import BulkWriteError


log = logging.getLogger(__name__)


class GeoTypeEnum(Enum):
    region = "region"
    raion = "raion"
    city = "city"
    district = "district"
    microdistrict = "microdistrict"
    street = "street"
    address = "address"


class AddressObj(typing.NamedTuple):
    geo_type: GeoTypeEnum
    geo_id: int
    name_ru: str
    name_uk: str


def _split_address(
        address: str, geo_type: GeoTypeEnum
) -> typing.Optional[dict]:
    (
        region, raion, city, district, microdistrict, street, street_number
    ) = (
        None, None, None, None, None, None, None
    )
    parts = list([
        p.strip() for p in
        address.split(",")
    ])
    if len(parts) == 1:
        if geo_type == GeoTypeEnum.raion:
            raion, = parts
        elif geo_type == GeoTypeEnum.region:
            region, = parts
        elif geo_type == GeoTypeEnum.city:
            city, = parts
        else:
            return log.error(
                "can not parse address %s with geo_type %s", address, geo_type
            )
    elif len(parts) == 2:
        if geo_type == GeoTypeEnum.raion:
            region, raion = parts
        elif geo_type == GeoTypeEnum.city:
            region, city = parts
        elif geo_type == GeoTypeEnum.district:
            city, district = parts
        elif geo_type == GeoTypeEnum.microdistrict:
            city, microdistrict = parts
        elif geo_type == GeoTypeEnum.street:
            city, street = parts
        else:
            return log.error(
                "can not parse address %s with geo_type %s", address, geo_type
            )
    elif len(parts) == 3:
        if geo_type == GeoTypeEnum.city:
            region, raion, city = parts
        elif geo_type == GeoTypeEnum.district:
            region, city, district = parts
        elif geo_type == GeoTypeEnum.microdistrict:
            region, city, microdistrict = parts
        elif geo_type == GeoTypeEnum.street:
            region, city, street = parts
        elif geo_type == GeoTypeEnum.address:
            city, street, street_number = parts
        else:
            return log.error(
                "can not parse address %s with geo_type %s", address, geo_type
            )
    elif len(parts) == 4:
        if geo_type == GeoTypeEnum.district:
            region, raion, city, district = parts
        elif geo_type == GeoTypeEnum.street:
            region, city, district, street = parts
        elif geo_type == GeoTypeEnum.address:
            region, city, street, street_number = parts
        else:
            return log.error(
                "can not parse address %s with geo_type %s", address, geo_type
            )
    elif len(parts) == 5:
        if geo_type == GeoTypeEnum.street:
            region, raion, city, district, street_number = parts
        elif geo_type == GeoTypeEnum.address:
            region, city, district, street, street_number = parts
        else:
            return log.error(
                "can not parse address %s with geo_type %s", address, geo_type
            )
    else:
        return log.error(
            "can not parse address %s with geo_type %s", address, geo_type
        )

    return {
        GeoTypeEnum.region.name: region,
        GeoTypeEnum.raion.name: raion,
        GeoTypeEnum.city.name: city,
        GeoTypeEnum.district.name: district,
        GeoTypeEnum.microdistrict.name: microdistrict,
        GeoTypeEnum.street.name: street,
        GeoTypeEnum.address.name: street_number,
    }


def _obj_to_mongo_doc(obj: AddressObj) -> dict:
    return {
        "_id": obj.geo_id,
        "geo_type": obj.geo_type.name,
        "name_ru": obj.name_ru,
        "name_uk": obj.name_uk,
        "analyzed_ru": _split_address(obj.name_ru, obj.geo_type),
        "analyzed_ua": _split_address(obj.name_uk, obj.geo_type),
    }


def _mongo_doc_to_obj(doc: dict) -> AddressObj:
    return AddressObj(
        geo_id=doc["_id"],
        geo_type=GeoTypeEnum(doc["geo_type"]),
        name_uk=doc["name_uk"],
        name_ru=doc["name_ru"],
    )

# region mongo-errors


MONGO_DUPLICATE_KEY_ERR = 11000

# endregion

MAX_LIMIT = 100


class DuplicateAddressException(Exception):
    pass


class Storage:
    def __init__(
        self, mongo_uri: str, mongo_db: str, mongo_collection: str,
    ):
        client = pymongo.MongoClient(mongo_uri)
        db = client[mongo_db]
        collection = db[mongo_collection]
        self.collection: pymongo.collection.Collection = collection

    def insert(self, objs: typing.Iterable[AddressObj]) -> typing.List[int]:
        """
        :return: inserted ids
        """
        try:
            result = self.collection.insert_many(
                map(_obj_to_mongo_doc, objs)
            )
        except BulkWriteError as exc:
            for err in exc.details.get('writeErrors', []):
                if err.get("code") == MONGO_DUPLICATE_KEY_ERR:
                    raise DuplicateAddressException(
                        f"Address object with same id already exists: "
                        f"{err.get('op')}"
                    )
            raise exc
        return result.inserted_ids

    def replace_one(self, geo_id: int, obj: AddressObj) -> typing.Optional[int]:
        """
        :return: updated id
        """

        found = self.collection.find_one_and_replace(
            {"_id": geo_id},
            _obj_to_mongo_doc(obj),
        )
        if found is None:
            return None
        return found["_id"]

    def list(self, limit=None, offset=None) -> typing.List[AddressObj]:
        if limit is None:
            limit = MAX_LIMIT
        if offset is None:
            offset = 0
        cursor: Cursor = self.collection.find(
            {}, sort=[('_id', pymongo.ASCENDING)]
        ).limit(
            limit
        ).skip(
            offset
        )
        return list(map(_mongo_doc_to_obj, cursor))

    def get(self, geo_id: int) -> typing.Optional[AddressObj]:
        result = self.collection.find_one({"_id": geo_id})
        if result is None:
            return None
        return _mongo_doc_to_obj(result)

    def search(self, term: str, limit: int):
        cursor: Cursor = self.collection.find(
            {"$text": {"$search": term}},
            {"score": {"$meta": "textScore"}}
        ).sort(
            [('score', {'$meta': 'textScore'})]
        ).limit(
            limit
        )
        return list(map(_mongo_doc_to_obj, cursor))

    def search2(self, term: str, limit: int):
        regexp = ".*".join(term.split(" "))
        cursor: Cursor = self.collection.find(
            {"$or": [
                {"name_ru": {"$regex": regexp}},
                {"name_ua": {"$regex": regexp}}
            ]}
        ).limit(limit)
        return list(map(_mongo_doc_to_obj, cursor))

    def search3(self, term: str, limit: int):
        regexp = ".*".join(term.split(" "))
        cursor: Cursor = self.collection.find(
            {"$and": [
                {"$text": {"$search": term}},
                {"$or": [
                    {"name_ru": {"$regex": regexp}},
                    {"name_ua": {"$regex": regexp}}
                ]}
            ]},
            {"score": {"$meta": "textScore"}}
        ).sort(
            [('score', {'$meta': 'textScore'})]
        ).limit(
            limit
        )
        return list(map(_mongo_doc_to_obj, cursor))
