import json
import typing
from http import HTTPStatus

from flask import Response


CONTENT_TYPE_JSON = "application/json"


def dump_json(resp, pretty=True):
    if pretty:
        return json.dumps(resp, indent=4, sort_keys=True) + "\n"
    else:
        return json.dumps(resp)


def abort(code, error):
    resp = {
        "status": "error",
        "code": code,
        "error": str(error)
    }
    return Response(
        response=dump_json(resp),
        status=code,
        content_type=CONTENT_TYPE_JSON,
    )


def bad_request(error):
    return abort(
        HTTPStatus.BAD_REQUEST,
        error,
    )


def not_found(error):
    return abort(
        HTTPStatus.NOT_FOUND,
        error,
    )


def conflict(error):
    return abort(
        HTTPStatus.CONFLICT,
        error,
    )


def ok(resp: typing.Union[dict, list]):
    return Response(
        response=dump_json(resp),
        status=HTTPStatus.OK,
        content_type=CONTENT_TYPE_JSON,
    )


def created(resp: typing.Union[dict, list]):
    return Response(
        response=dump_json(resp),
        status=HTTPStatus.CREATED,
        content_type=CONTENT_TYPE_JSON,
    )


def server_error(exc: typing.Optional[Exception] = None):
    if exc is None:
        error = "Internal Error"
    else:
        error = str(exc)
    return abort(HTTPStatus.INTERNAL_SERVER_ERROR, error)


def unexpected_content_type(
    got_content_type,
    expected_content_types,
):
    return abort(
        HTTPStatus.UNSUPPORTED_MEDIA_TYPE,
        f"expected Content-Types: {expected_content_types}; "
        f"got: {got_content_type}"
    )
