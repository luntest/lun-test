import os
from flask import Flask
from address_suggester.lib.storage import Storage
from address_suggester.lib.utils_web import server_error

address_storage = Storage(
    os.environ.get("MONGO_URI", "mongodb://root:example@localhost:27017/"),
    os.environ.get("MONGO_DB", "geo"),
    os.environ.get("MONGO_COLLECTION", "objects"),
)


def main():
    from address_suggester.api.controllers import api_v1
    app = Flask(__name__)
    app.register_blueprint(api_v1, url_prefix="/v1/address")
    app.errorhandler(500)(server_error)
    app.run(host="0.0.0.0", debug=False)


if __name__ == "__main__":
    main()
