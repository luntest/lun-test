import typing
import csv
from io import StringIO

import jsonschema
import logging
from flask import (
    Blueprint,
    request,
)

from address_suggester.lib.storage import GeoTypeEnum, AddressObj, DuplicateAddressException
from address_suggester.lib.utils_web import (
    ok, bad_request, created,
    conflict,
    unexpected_content_type, server_error,
    not_found,
)

from address_suggester.app import address_storage


log = logging.getLogger(__name__)

api_v1 = Blueprint("api_v1", "api_v1")


@api_v1.route("/bulk-insert", methods=["POST"])
def csv_bulk_insert():
    if request.content_type != "application/csv":
        return unexpected_content_type(
            request.content_type, ["application/csv"]
        )
    try:
        objects = _deserialize_csv(StringIO(request.data.decode()))
    except CsvParsingException as exc:
        return bad_request(f"can not parse csv: {str(exc)}")
    try:
        ids = address_storage.insert(objects)
    except DuplicateAddressException as exc:
        return conflict(exc)
    return ok(ids)


@api_v1.route("", methods=['POST'])
def insert_address():
    try:
        obj = _deserialize_address_obj(request.json)
    except jsonschema.ValidationError as exc:
        return bad_request(exc.message)
    try:
        ids = address_storage.insert([obj])
    except DuplicateAddressException as exc:
        return conflict(exc)
    if len(ids) != 1:
        return server_error()
    return created({"geo_id": ids[0]})


@api_v1.route("", methods=["GET"])
def list_addresses():
    limit, offset = None, None
    if "limit" in request.args:
        try:
            limit = int(request.args["limit"])
        except ValueError:
            return bad_request("limit param should be an integer")
    if "offset" in request.args:
        try:
            offset = int(request.args["offset"])
        except ValueError:
            return bad_request("offset param should be an integer")

    return ok(
        list(map(
            _serialize_address_obj,
            address_storage.list(limit, offset)
        ))
    )


@api_v1.route("/<int:geo_id>", methods=["PUT"])
def update_address(geo_id: int):
    try:
        obj = _deserialize_address_obj(request.json)
    except jsonschema.ValidationError as exc:
        return bad_request(exc.message)
    if geo_id != obj.geo_id:
        return bad_request("can not change geo_id field")

    obj_id = address_storage.replace_one(geo_id, obj)
    if obj is None:
        return not_found("address not found")

    return ok({"id": obj_id})


@api_v1.route("/<int:geo_id>", methods=["GET"])
def get_address(geo_id: int):
    obj = address_storage.get(geo_id)
    if obj is None:
        return not_found("address not found")
    return ok(_serialize_address_obj(obj))


@api_v1.route("/search", methods=["GET"])
def search():
    if "q" not in request.args:
        return bad_request("you should provide search term as `q` argument")
    term = request.args["q"]
    limit = int(request.args.get("limit", "5"))
    result = address_storage.search(term, limit)
    return ok(list(map(_serialize_address_obj, result)))


@api_v1.route("/search2", methods=["GET"])
def search2():
    if "q" not in request.args:
        return bad_request("you should provide search term as `q` argument")
    term = request.args["q"]
    limit = int(request.args.get("limit", "5"))
    result = address_storage.search2(term, limit)
    return ok(list(map(_serialize_address_obj, result)))


@api_v1.route("/search3", methods=["GET"])
def search3():
    if "q" not in request.args:
        return bad_request("you should provide search term as `q` argument")
    term = request.args["q"]
    limit = int(request.args.get("limit", "5"))
    result = address_storage.search3(term, limit)
    return ok(list(map(_serialize_address_obj, result)))

# region helpers


def _deserialize_address_obj(obj: dict) -> AddressObj:
    schema = {
        "type": "object",
        "properties": {
            "geo_id": {"type": "number"},
            "geo_type": {"enum": list(e.name for e in GeoTypeEnum)},
            "name_ru": {"type": "string"},
            "name_uk": {"type": "string"},
        },
        "required": ["geo_id", "geo_type", "name_ru", "name_uk"]
    }
    jsonschema.validate(obj, schema)
    return AddressObj(
        geo_id=obj["geo_id"],
        geo_type=GeoTypeEnum(obj["geo_type"]),
        name_ru=obj["name_ru"],
        name_uk=obj["name_uk"],
    )


def _serialize_address_obj(obj: AddressObj) -> dict:
    return {
        "geo_id": obj.geo_id,
        "geo_type": obj.geo_type.name,
        "name_ru": obj.name_ru,
        "name_uk": obj.name_uk,
    }


class CsvParsingException(Exception):
    pass


def _deserialize_csv(csv_file) -> typing.List[AddressObj]:
    reader = csv.reader(csv_file, quotechar='"')
    (col_geo_id, col_geo_type, col_name_ru, col_name_uk) = [
        None, None, None, None
    ]
    result = []
    for idx, row in enumerate(reader):
        if idx == 0:
            for col_idx, col in enumerate(row):
                if col == "geo_id":
                    col_geo_id = col_idx
                elif col == "geo_type":
                    col_geo_type = col_idx
                elif col == "name":
                    col_name_ru = col_idx
                elif col == "name_uk":
                    col_name_uk = col_idx
            if any(h is None for h in (
                col_geo_id, col_geo_type, col_name_ru, col_name_uk
            )):
                raise CsvParsingException(
                    "not all headers provided in first row"
                )
            continue

        if len(row) < 4:
            raise CsvParsingException(f"len of row {idx} < headers len")
        try:
            geo_id = int(row[col_geo_id])
        except ValueError:
            raise CsvParsingException("geo_id should be an integer number")

        try:
            geo_type = GeoTypeEnum(row[col_geo_type])
        except ValueError:
            raise CsvParsingException(
                f"geo_type should be one of: "
                f"{list(e.name for e in GeoTypeEnum)}"
            )
        result.append(AddressObj(
            geo_id=geo_id,
            geo_type=geo_type,
            name_ru=row[col_name_ru],
            name_uk=row[col_name_uk],
        ))

    return result
# endregion
